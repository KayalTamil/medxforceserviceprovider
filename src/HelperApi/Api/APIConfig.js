import Config from './Config'

let url = Config.authUrl;
let dashUrl = Config.dashUrl;
let profileUrl = Config.profileUrl;
let agoraUrl = Config.agoraUrl

export const RegisterUrl = url + "register";
export const LoginUrl = url + "mobile/login/";
export const LoginOtpUrl = url + "mobile/login/verify/";
export const RegisterMobileUrl = url + "mobile/registration?phone=";
export const RegisterMobileVerifyUrl = url + "mobile/registration/verify?";
export const CategoryUrl = dashUrl + 'services/findNames';
export const LocationUrl = dashUrl + 'person/location';
export const OrdersUrl = dashUrl+'order';
export const OrdersUpdateUrl = dashUrl + 'order/update';
export const GetPriceUrl = dashUrl + 'services/getPricesByPersonId?';
export const ProfileGetUrl = profileUrl +'profile';
export const ToggelUrl = profileUrl +'toggle/';
export const RegisterOtpUrl = url + "mobile/otp/+919999999999";
export const GetImageUrl = profileUrl + "getImage";
export const AgoraSdkUrl = agoraUrl;
export const AgoraRetriveUrl = 'https://medxforce.herokuapp.com/medx/api/agora/retrieveToken';
export const PersonUrl = dashUrl + 'service?';
export const ExpertiseUrl = dashUrl + 'getExpertiseList';
