export default {
    authUrl : "https://medxforce.herokuapp.com/medx/api/auth/",
    dashUrl : "https://medxforce.herokuapp.com/medx/api/dashboard/",
    orderUrl :"https://medxforce.herokuapp.com/medx/api/order",
    profileUrl :"https://medxforce.herokuapp.com/medx/api/person/",
    agoraUrl :"https://medxforce.herokuapp.com/medx/api/agora/retrieveToken"
}