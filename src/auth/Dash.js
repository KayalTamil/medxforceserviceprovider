import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image, StyleSheet, BackHandler, TouchableOpacity, TextInput, Alert, Modal, ScrollView
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

import SplashScreen from 'react-native-splash-screen';

class Dash extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      isModalVisible: false,
      isOpacity: false,
    };
  }

  componentDidMount() {
    SplashScreen.hide();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    

    AsyncStorage.getItem('login', (err, login) => {
      if (login != null) {
        var keyData = JSON.parse(login);
        if (keyData.subscriptionisExpired != 0 || keyData.subscriptionisExpired === false) {
          this.props.navigation.navigate('Home')
        } else {
          if (keyData.subscriptionisExpired) {
            this.setState({
              isModalVisible: true,
              isOpacity: true
            })
          } else {
            this.props.navigation.navigate('SubScription')
          }
        }
      } else {
        this.props.navigation.navigate('Login')
      }
    });
  }

  componentWillUnmount() {
    SplashScreen.hide();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }
  
  onOkHandle = () => {
    this.setState({ isModalVisible: false, isOpacity: false }, () => {
      this.props.navigation.navigate('SubScription')
    })
  }
  handleBackPress = () => {
    Alert.alert(
      '',
      'Are you sure you want to exit the application ?', [{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },], {
      cancelable: false
    }
    )
    return true;
  };

  render() {
    let { isOpacity } = this.state;
    return (
      <>
        <StatusBar style={{ backgroundColor: Colors.Primary }} />
        <ScrollView style={isOpacity && { backgroundColor: Colors.Primary, opacity: 0.2 }}>
          <View style={styles.centeredView}>
            <Modal
              animated
              animationType="slide"
              transparent={true}
              visible={this.state.isModalVisible}
              style={{
                opacity: 0.5,
              }}
              onRequestClose={() => {
                this.setState({ isModalVisible: !this.state.isModalVisible, isOpacity: false })
              }}>
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <View style={{ height: Dims.DeviceHeight / 2.5, width: Dims.DeviceWidth - 40, borderTopLeftRadius: 60, borderTopRightRadius: 60, alignSelf: 'center' }}>
                    <Image source={require('../assets/images/payment.png')} style={{ alignSelf: 'center', height: 120, width: 120 }} />
                    <Text style={{ fontFamily: 'Poppins-Bold', textAlign: 'center', margin: 10, fontSize: 15 }}>Your Subscription is expierd</Text>
                    <Text style={{ fontFamily: 'Poppins-Medium', textAlign: 'center', margin: 10, fontSize: 13 }}>Please renewal to your offer services.</Text>
                    <TouchableOpacity style={{ backgroundColor: Colors.orangeColor, width: 150, borderRadius: 30, alignSelf: 'center', top: 10 }} onPress={() => { this.onOkHandle() }}>
                      <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10 }}>Continue</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
  },
  modalView: {
    marginHorizontal: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    shadowColor: Colors.greyColor,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});

export default Dash;
