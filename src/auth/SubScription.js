import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, TextInput, FlatList, KeyboardAvoidingView, ScrollView, Modal, ActivityIndicator
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';
import { HttpHelper, HttpFavHelper } from '../HelperApi/Api/HTTPHelper';
import { OrdersUrl, OrdersUpdateUrl, GetPriceUrl,ProfileGetUrl } from '../HelperApi/Api/APIConfig';
import RazorpayCheckout from 'react-native-razorpay';


class SubScription extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            mobileNumber: '1234567890',
            otpInput: '',
            subscripionPlanData: [],
            accessKey: '',
            paymentDetails: {},
            isModalVisible: false,
            isOpacity: false,
            personId: '',
            isLoading: false,
            name:'',
            paymentCancelled:''
        };
    }

    componentDidMount() {
        SplashScreen.hide();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        AsyncStorage.getItem('login', (err, login) => {
            if (login != null) {
                var keyData = JSON.parse(login);
                this.setState({
                    accessKey: keyData.accessToken,
                    personId: keyData.personId
                }, () => {
                    let priceData = HttpHelper(`${GetPriceUrl}personId=${keyData.personId}`, 'GET', '');
                    priceData.then(response => {
                        if (response)
                            this.setState({
                                subscripionPlanData: response
                            })
                    })
                    let profileData = HttpFavHelper(`${ProfileGetUrl}`, 'GET', '',this.state.accessKey);
                    profileData.then(response => {
                        if (response)
                            this.setState({
                                profileData: response,
                                name:response.firstName +' '+response.lastName
                            })
                    })
                })
            }
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Login')
        return true;
    };
    onHandlePayment = (item) => {
        let { personId } = this.state;
        let period = item.month.slice(0, 2)
        this.setState({
            isLoading: true,
            subId: item.id
        })
        let orderData = HttpFavHelper(`${OrdersUrl}?period=${period}&personId=${personId}`, 'POST', '', this.state.accessKey);
        orderData.then(response => {
            const options = {
                "key": response.secretKey,
                "amount": (item.amount * 100).toString(),
                "currency": 'INR',
                "name": "MedXForce",
                "description": item.serviceName +' for '+ item.month,
                "image": "https://example.com/your_logo",
                "order_id": response.razorpayOrderId,
                "prefill": {
                    "name": response.personName,
                    "email": response.personEmail,
                    "contact": response.personContact,
                },
            };
            RazorpayCheckout.open(options).then(async data => {
                if (data && data.razorpay_payment_id) {
                    await this.setState({ paymentDetails: data }, () => {
                        this.placeOrder();
                    });
                }
            }).catch((error) => {
                this.setState({
                    isModalVisible:true,
                    isOpacity:true,
                    paymentCancelled :"Payment processing cancelled by user",

                })
                // alert(`Error: ${error.code} | ${error.description}`);
            });
        })
    }
    placeOrder = () => {
        let { paymentDetails, accessKey, personId } = this.state;
        let body = {
            "razorpayOrderId": paymentDetails.razorpay_order_id,
            "razorpayPaymentId": paymentDetails.razorpay_payment_id,
            "razorpaySignature": paymentDetails.razorpay_signature,
            "personId": personId
        }
        body = JSON.stringify(body)
        let orderData = HttpFavHelper(`${OrdersUpdateUrl}`, 'POST', body, accessKey);
        orderData.then(res => {
            if (res && res.success === true) {
                this.setState({
                    isModalVisible: true,
                    isOpacity: true,
                    isLoading: false
                }, () => {
                    try {
                        AsyncStorage.setItem('payment', JSON.stringify(res), (err) => {
                        });
                    } catch (error) { }
                })
            }

        })
    }
    onOkHandle = () => {
        this.setState({ isModalVisible: false, isOpacity: false }, () => {
            this.props.navigation.navigate("Location")
        })
    }
    _renderSectorItem = (item, index) => {
        return (
            <>
                <TouchableOpacity onPress={() => { this.onHandlePayment(item) }} style={{ flexDirection: 'column', backgroundColor: Colors.White, padding: 10, marginTop: 10, borderRadius: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12 }}>{item.month}</Text>
                        <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 8, color: Colors.redColor }}>{'INAUGURAL OFFER'}</Text>
                    </View>
                    <Text style={{ alignSelf: 'flex-end' }}>
                        {this.state.subId === item.id ? <ActivityIndicator size="small" color={Colors.Primary} /> :
                            <MaterialIcons
                                name="arrow-forward-ios"
                                size={20}
                                color={Colors.Primary}
                            />
                        }
                    </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 20, color: Colors.Primary, letterSpacing: 3 }}><Text style={{ fontFamily: 'none' }}>{'\u20B9'}</Text>{item.amount}</Text>
                        <Text style={{ fontFamily: 'Poppins-Medium', left: 15, top: 8, fontSize: 12, textDecorationLine: 'line-through', letterSpacing: 3 }}><Text style={{ fontFamily: 'none' }}>{'\u20B9'}</Text>{item.originalAmount}</Text>
                    </View>
                </TouchableOpacity>

            </>

        )
    }

    render() {
        let { isOpacity,paymentCancelled } = this.state;
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <ScrollView style={isOpacity && { backgroundColor: Colors.Primary, opacity: 0.2 }} contentInsetAdjustmentBehavior="automatic" keyboardShouldPersistTaps="always"  >
                    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "position" : null} style={{ flex: 1 }}>
                        <View>
                            <View style={[styles.dotView, { justifyContent: 'space-between' }]}>
                                <TouchableOpacity onPress={() => { this.handleBackPress() }} style={[styles.dotView, { alignSelf: 'center' }]}>
                                    <Text style={{ marginLeft: 15 }}>
                                        <MaterialIcons
                                            name="verified-user"
                                            size={20}
                                            color={Colors.Primary}
                                        />
                                    </Text>
                                    <Text style={{ fontFamily: 'Poppins-Bold', color: Colors.secondaryTextColor, marginLeft: 5, fontSize: 12, letterSpacing: 1 }}>
                                        VERIFIED
                                    </Text>
                                </TouchableOpacity>
                                <View style={styles.dotView}>
                                    <Entypo
                                        name="dot-single"
                                        size={40}
                                        color={Colors.Primary}
                                    />
                                    <Entypo
                                        name="dot-single"
                                        size={40}
                                        color={Colors.Primary}
                                        style={{ marginLeft: -15 }}
                                    />
                                    <Entypo
                                        name="dot-single"
                                        size={40}
                                        color={Colors.Primary}
                                        style={{ marginLeft: -15 }}
                                    />
                                </View>
                            </View>
                            <View style={{ justifyContent: 'space-evenly', flexDirection: 'row', paddingHorizontal: 20, }}>
                                <View style={{ flex: 0.5, top: 30 }}>
                                    <Text style={{ fontFamily: 'Poppins-Bold', color: Colors.Primary }}>Hi {this.state.name},</Text>
                                    <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.placeholderColor, fontSize: 9.5 }}>Quote your hourly pricing per visit.</Text>
                                    <View style={{ flexDirection: 'row', }}>
                                        <TextInput
                                            style={styles.inputText1}
                                            editable={false}
                                            defaultValue={'\u20B9'}
                                        />
                                        <TextInput
                                            keyboardType="number-pad"
                                            style={styles.inputText}
                                            placeholder={'Price.'}
                                            autoCapitalize="none"
                                            returnKeyType="next"
                                            placeholderTextColor={Colors.lightWhiteColor}
                                            onChangeText={(email) => this.setState({ email })}
                                        />
                                    </View>

                                </View>
                                <View style={{ flex: 0.5, }}>
                                    <Image
                                        source={require('../assets/images/nurseRight.png')}
                                        style={[styles.nurseLogo, { width: 200, height: 300 }]}
                                        resizeMode="cover"
                                    />
                                </View>
                            </View>
                            <View style={{ backgroundColor: Colors.Primary, borderTopLeftRadius: 60, borderTopRightRadius: 60, marginTop: -70, }}>
                                <Text style={{ fontFamily: 'Poppins-ExtraBold', letterSpacing: 7, color: Colors.White, textAlign: 'center', margin: 20, }}>SUBSCRIPTION</Text>
                                <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10, letterSpacing: 1 }}>Select a plan to activate {'\n'}your account</Text>
                                <FlatList
                                    style={{ paddingHorizontal: 20, borderRadius: 30, height: Dims.DeviceHeight / 2 }}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={({ item, index }) => this._renderSectorItem(item, index)}
                                    data={this.state.subscripionPlanData}
                                />
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                    <View style={styles.centeredView}>
                        <Modal
                            animated
                            animationType="slide"
                            transparent={true}
                            visible={this.state.isModalVisible}
                            style={{
                                opacity: 0.5,
                            }}
                            onRequestClose={() => {
                                this.setState({ isModalVisible: !this.state.isModalVisible, isOpacity: false })
                            }}>
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <View style={{ height: Dims.DeviceHeight / 2.5, width: Dims.DeviceWidth - 40, borderTopLeftRadius: 60, borderTopRightRadius: 60, alignSelf: 'center' }}>
                                        <Image source={paymentCancelled ?require('../assets/images/cancel.jpeg'): require('../assets/images/payment.png')} style={{ alignSelf: 'center', height: 120, width: 120 }} />
                                        <Text style={paymentCancelled ?{ fontFamily: 'Poppins-Bold', textAlign: 'center', margin: 10, fontSize: 15,color:Colors.redColor }:{ fontFamily: 'Poppins-Bold', textAlign: 'center', margin: 10, fontSize: 15 }}>{paymentCancelled ?paymentCancelled : 'Payment SuccessFully'}</Text>
                                        <Text style={{ fontFamily: 'Poppins-Medium', textAlign: 'center', margin: 10, fontSize: 13 }}>Please continue to  your offer services.</Text>
                                        <TouchableOpacity style={{ backgroundColor: Colors.orangeColor, width: 150, borderRadius: 30, alignSelf: 'center', top: 10 }} onPress={() => { this.onOkHandle() }}>
                                            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10 }}>Continue</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>
                </ScrollView>
                <View style={{
                    alignItems: 'center',
                    position: 'absolute',
                    flex: 1,
                    bottom: 0,
                    width: '100%',
                    backgroundColor: Colors.Primary,
                    height: 30,
                    justifyContent: 'center'
                }}>
                    {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('LoginVerify') }} style={{ flexDirection: 'row', justifyContent: 'center', height: 50 }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.White, width: 210 }}></Text>
                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.White }}>Pay Later</Text>
                        <Text style={{ width: 30, height: 30, borderRadius: 50, backgroundColor: Colors.White, textAlign: 'center', left: 20, top: -5 }}>
                            <MaterialIcons
                                name="arrow-right-alt"
                                size={28}
                                color={Colors.Primary}
                            // style={{ marginLeft: -15 }}
                            /></Text>
                    </TouchableOpacity> */}
                    <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, fontSize: 9 }}>By continuing you agree to our <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Terms & Conditions</Text> and <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Payment Policy</Text> </Text>

                </View>
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    dotView: {
        flexDirection: 'row',
    },
    backView: {
        flexDirection: 'row',
    },
    logo: {
        height: Dims.DeviceHeight / 3,
        width: Dims.DeviceWidth - 50
    },
    headerLogo: {
        height: 25,
        width: 150,
    },
    nurseLogo: {
        // height: 300,
        // left: -170
    },
    welcomeText: { fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.Primary, letterSpacing: 4, top: 10 },
    defaultInput: {
        color: Colors.Black,
        fontSize: 12,
        top: 10,
        justifyContent: 'center',

    },
    textInputView: {
        paddingLeft: 15,
    },
    container: {
        flexDirection: 'row',
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',

    },
    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        flex: 0.9
    },
    headerFooterContainer: {
        alignItems: 'center',
    },
    inputText: {
        width: 100,
        height: 40,
        fontSize: 12,
        // backgroundColor: '#fcfcfc',
        fontFamily: 'Poppins-Medium',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        backgroundColor: Colors.Primary,
        color: Colors.White,
        alignSelf: 'center',
    },
    inputText1: {
        alignSelf: 'center',
        width: 40,
        height: 40,
        fontSize: 16,
        // fontFamily: 'Poppins-Medium',
        textAlign: 'center',
        backgroundColor: '#fcfcfc',
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        backgroundColor: Colors.Primary,
        color: Colors.White
    },
    textInputContainer: {
        marginHorizontal: 40,
    },
    roundedTextInput: {
        height: 70,
        fontFamily: 'Poppins-Medium'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        // alignItems: "center",
        // marginTop: 22,
        // marginHorizontal: 10,
    },
    modalView: {
        marginHorizontal: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        // alignItems: "center",
        shadowColor: Colors.greyColor,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,


    },
});

export default SubScription;
