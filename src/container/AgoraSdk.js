import React, { Component } from 'react';
import { Text, TextInput, Button, ActivityIndicator, DeviceEventEmitter, Platform, View, BackHandler, FlatList, StatusBar, ImageBackground, TouchableNativeFeedback, SafeAreaView, Keyboard, Alert, KeyboardAvoidingView, PermissionsAndroid, StyleSheet, Image, TouchableOpacity, Dimensions, ViewPropTypes } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MapView, {
  AnimatedRegion,
  PROVIDER_GOOGLE,
  Marker,
} from "react-native-maps";
import { Colors } from '../assets/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { Dims } from '../components/Dims';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
const googleApiKey = 'AIzaSyAFn3Q1LhhC0C8pfWhNyHwfHWYFT7S2s_M';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import BackgroundTimer from 'react-native-background-timer';

import Footer from '../components/Footer';
import SplashScreen from 'react-native-splash-screen';
import DeviceInfo from 'react-native-device-info';
import RtcEngine, {
  ChannelProfile,
  ClientRole,
  RtcEngineContext, RtcLocalView, RtcRemoteView
} from 'react-native-agora';
import { HttpHelper, HttpFavHelper } from '../HelperApi/Api/HTTPHelper';
import { AgoraUrl, AgoraRetriveUrl, ProfileGetUrl } from '../HelperApi/Api/APIConfig';
import "react-native-get-random-values";
import { v4 as uuid } from "uuid";
const ASPECT_RATIO = Dims.DeviceWidth / Dims.DeviceHeight;
const LATITUDE_DELTA = 0.039999;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const CARD_HEIGHT = Dims.DeviceHeight / 6;
const CARD_WIDTH = CARD_HEIGHT - 50;
var initialRegion;
var markerRegion;

BackgroundTimer.start(2000); // delay in milliseconds only for Android


class map extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {

      markerData: {
        latitude: 35.1790507,
        longitude: -6.1389008,
      },
      keyboardState: 'closed',
      getAddress: [],
      isChangeLocation: false,
      changeData: [],
      address1: '',
      address2: '',
      city: '',
      state: '',
      pinCode: '',
      getDataAddress: [],
      getIconData: params && params.getBookData ? params.getBookData : [],
      selectedData: [],
      rippleColor: Colors.blueColor,
      rippleOverflow: false,
      appId: '49c0dbb169294ddca113f24be7f2d848',
      token: '00649c0dbb169294ddca113f24be7f2d848IADONZuXud8r/Ow/LPgMjZOYex96SGTjARfetTWii86FHgx+f9gAAAAAEAC7nPWLRor2YQEAAQBGivZh',
      channelName: 'test',
      joinSucceed: false,
      peerIds: [],
      uid: 123456,
      isJoined: false,
      openMicrophone: true,
      enableSpeakerphone: true,
      playEffect: false,
      channelId: '',
      getTokenData: {},
      _remoteUid: null,
      type: "",
      providerId: params && params.message ? params.message.data.providerId : "",
      userName: params && params.message ? params.message.data.customerName : "",
      serviceName: params && params.message ? params.message.data.serviceName : "",
    };
  }

  componentDidMount() {
    SplashScreen.hide();
    this.requestCameraAndAudioPermission()
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this.createUUID()
    this._initEngine();
    this.getProfileData()
    DeviceEventEmitter.addListener('backgroundTimer', () => {
      // this will be executed once after 5 seconds
    });
  }
  async requestCameraAndAudioPermission() {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      ]);
      if (
        granted["android.permission.RECORD_AUDIO"] === PermissionsAndroid.RESULTS.GRANTED &&
        granted["android.permission.CAMERA"] === PermissionsAndroid.RESULTS.GRANTED
      ) {
      } else {
        console.log("Permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  createUUID = () => {
    let createUid = uuid();
  }

  getProfileData = () => {
    AsyncStorage.getItem('login', (err, login) => {

      if (login != null) {
        var keyData = JSON.parse(login);
        this.setState({
          accessKey: keyData.accessToken,
          personId: keyData.personId
        }, () => {
          let profileData = HttpFavHelper(`${ProfileGetUrl}`, 'GET', '', this.state.accessKey);
          profileData.then(response => {
            if (response)
              this.setState({
                profileData: response,
                name: response.firstName + ' ' + response.lastName
              })
          })
        })
      }
    });
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);

    SplashScreen.hide();
    this._engine?.destroy();
  }
  _initEngine = async () => {
    let { appId } = this.state;
    this._engine = await RtcEngine.create(appId)
    await this._engine.enableAudio();
    this._addListeners();
    await this._engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await this._engine.setClientRole(ClientRole.Broadcaster);
  };

  _addListeners = () => {
    let { type } = this.state;
    this._engine.addListener('UserOffline', (uid, reason) => {
      const { peerIds } = this.state;
      this.setState({
          // Remove peer ID from state array
          peerIds: peerIds.filter((id) => id !== uid),
          _remoteUid: null, isModalVisible: true,
          isOpacity: true,
      }, () => {
          // this.props.navigation.navigate('OrderStatus', { getData: this.state.getIconData, isModal: true })

      })
  });
    this._engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      this.setState({ isJoined: true, isSelect: false }, () => {
        // this.props.navigation.navigate('CallScreen', { type: type, uuid: this.state._remoteUid, status: this.state.isJoined,selectedData:this.state.selectedData })
      });
    });
    this._engine.addListener('LeaveChannel', (stats) => {
      this.setState({ isJoined: false }, () => {
        this.props.navigation.navigate('Home')
      });
    });
  };


  _joinChannel = (type) => {
    let { profileData, providerId } = this.state;
    let getToken = HttpHelper(`${AgoraRetriveUrl}/${providerId}`, 'GET', '');
    getToken.then(response => {
      if (response) {
        this.setState({
          getTokenData: response,
          type: type
        }, () => {
          let { getTokenData } = this.state;
          this._engine.joinChannel(
            getTokenData.token,
            getTokenData.channelName,
            getTokenData.role,
            getTokenData.uid
          );
        })
      }
    })
  }
  _onChangeRecordingVolume = (value) => {
    this._engine?.adjustRecordingSignalVolume(value * 400);
  };

  _onChangePlaybackVolume = (value) => {
    this._engine?.adjustPlaybackSignalVolume(value * 400);
  };

  _toggleInEarMonitoring = (isEnabled) => {
    this._engine?.enableInEarMonitoring(isEnabled);
  };

  _onChangeInEarMonitoringVolume = (value) => {
    this._engine?.setInEarMonitoringVolume(value * 400);
  };

  _leaveChannel = async () => {
    console.log('lll');
    await this._engine.leaveChannel();
    this.setState({
      isModalVisible: true,
      isOpacity: true,
    })
  };
  _switchMicrophone = () => {
    const { openMicrophone } = this.state;
    this._engine
      ?.enableLocalAudio(!openMicrophone)
      .then(() => {
        this.setState({ openMicrophone: !openMicrophone });
      })
      .catch((err) => {
        console.warn('enableLocalAudio', err);
      });
  };

  _switchSpeakerphone = () => {
    const { enableSpeakerphone } = this.state;
    this._engine
      ?.setEnableSpeakerphone(!enableSpeakerphone)
      .then(() => {
        this.setState({ enableSpeakerphone: !enableSpeakerphone });
      })
      .catch((err) => {
        console.warn('setEnableSpeakerphone', err);
      });
  };
  _switchEffect = () => {
    const { playEffect } = this.state;
    if (playEffect) {
      this._engine.stopEffect(1)
        .then(() => {
          this.setState({ playEffect: false });
        })
        .catch((err) => {
          console.warn('stopEffect', err);
        });
    } else {
      this._engine.playEffect(
          1,
          Platform.OS === 'ios'
            ? `${RNFS.MainBundlePath}/Sound_Horizon.mp3`
            : '/assets/Sound_Horizon.mp3',
          -1,
          1,
          1,
          100,
          true,
          0
        )
        .then(() => {
          this.setState({ playEffect: true });
        })
        .catch((err) => {
          console.warn('playEffect', err);
        });
    }
  };


  handleBackPress = () => {
    this.props.navigation.navigate('Home')
    return true;
  };
  async requestPermissions() {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, PermissionsAndroid.PERMISSIONS.RECORD_AUDIO],
      );
      this.getCurrentLocation();
    } catch (err) {

    }
  }


  render() {
    let { enableSpeakerphone, openMicrophone, isJoined, _remoteUid, getAddress, getDataAddress, locationAddress, selectedData, rippleColor, rippleOverflow, userName, serviceName } = this.state
    return (
      <>
        <StatusBar
          backgroundColor={Colors.Primary}
          barStyle="light-content"
        />
        <SafeAreaView backgroundColor={Colors.Primary}></SafeAreaView>
        <View style={{ backgroundColor: 'black' }}>

          <View style={{ width: Dims.DeviceWidth, height: Dims.DeviceHeight, backgroundColor: 'black' }}>
            <Image style={{ height: 40, width: 40, alignSelf: 'center', marginTop: 50, borderColor: Colors.White, borderWidth: 1, borderRadius: 200 }}
              source={require('../assets/images/logo.png')} />
            <View style={{ position: 'absolute', width: Dims.DeviceWidth, paddingHorizontal: 10, borderTopLeftRadius: 30, borderTopRightRadius: 30, top: 100 }}>
              <Text style={{ fontSize: 25, fontFamily: 'Poppins-Bold', alignSelf: 'center', color: Colors.White }}> {userName}</Text>
              <Text style={{ fontSize: 18, fontFamily: 'Poppins-Bold', alignSelf: 'center', color: Colors.White }}>{serviceName}</Text>
            </View>
            {isJoined ? (
              <View style={{ padding: 20, position: 'absolute', width: Dims.DeviceWidth, paddingHorizontal: 10, borderTopLeftRadius: 30, borderTopRightRadius: 30, bottom: 70, justifyContent: 'space-between' }}>
                <TouchableOpacity onPress={this._leaveChannel} style={{ alignSelf: 'center', justifyContent: 'center', height: 80, width: 80, borderRadius: 100, backgroundColor: 'red' }}>
                  <Text style={{ textAlign: 'center', textAlignVertical: 'center' }}>
                    <MaterialIcons
                      name={"call-end"}
                      size={35}
                      color={Colors.White}
                      style={{}}
                    /></Text>
                </TouchableOpacity>
                {/* <View style={{ padding: 20, justifyContent: 'space-between', flexDirection: 'row' }}>
                  <TouchableOpacity onPress={this._joinChannel} style={{ alignSelf: 'center', justifyContent: 'center', height: 80, width: 80, borderRadius: 100, }}>
                    <Text style={{ textAlign: 'center', textAlignVertical: 'center' }}>
                      <FontAwesome
                        name={!openMicrophone ? "microphone" : "microphone-slash"}
                        size={35}
                        color={Colors.White}
                        style={{}}
                      /></Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={this._leaveChannel} style={{ alignSelf: 'center', justifyContent: 'center', height: 80, width: 80, borderRadius: 100, }}>
                    <Text style={{ textAlign: 'center', textAlignVertical: 'center' }}>
                      <FontAwesome5

                        name={!enableSpeakerphone ? "volume-down" : "volume-mute"}
                        size={35}
                        color={Colors.White}
                        style={{}}
                      /></Text>
                  </TouchableOpacity>
                </View> */}
              </View>
            ) : (
              <View style={{ padding: 20, flexDirection: 'row', position: 'absolute', width: Dims.DeviceWidth, paddingHorizontal: 10, borderTopLeftRadius: 30, borderTopRightRadius: 30, bottom: 70, justifyContent: 'space-between' }}>
                <TouchableOpacity onPress={this._joinChannel} style={{ alignSelf: 'center', justifyContent: 'center', height: 80, width: 80, borderRadius: 100, backgroundColor: 'green' }}>
                  <Text style={{ textAlign: 'center', textAlignVertical: 'center' }}>
                    <MaterialIcons
                      name={"call"}
                      size={35}
                      color={Colors.White}
                      style={{}}
                    /></Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this._leaveChannel} style={{ alignSelf: 'center', justifyContent: 'center', height: 80, width: 80, borderRadius: 100, backgroundColor: 'red' }}>
                  <Text style={{ textAlign: 'center', textAlignVertical: 'center' }}>
                    <MaterialIcons
                      name={"call-end"}
                      size={35}
                      color={Colors.White}
                      style={{}}
                    /></Text>
                </TouchableOpacity>
              </View>
            )}
          </View>


          {/* <View style={{ flex: 1 }}>

            <Footer navigation={this.props.navigation} discountData={this.state.discountData} nearByData={this.state.nearByShopData} />
          </View> */}

        </View>
        <DropdownAlert
          ref={(ref) => (this.dropdown = ref)}
          containerStyle={{ backgroundColor: '#FF0000' }}
          imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
        />
      </>
    );

  };
}
export default map;
const styles = StyleSheet.create({
  deliver: {
    position: 'relative',
    bottom: 0,
    backgroundColor: Colors.Primary,
    borderTopLeftRadius: 60,
    borderTopRightRadius: 60,
    height: Dims.DeviceHeight / 2.5,
    width: Dims.DeviceWidth,
    // top:-30
    marginTop: -40
  },
  deliver1: {
    position: 'absolute',
    flex: 1,
    bottom: 0,
    top: 30,
    backgroundColor: Colors.Primary,
    borderTopLeftRadius: 60,
    borderTopRightRadius: 60,
    height: Dims.DeviceHeight / 0.2,
    width: Dims.DeviceWidth,
    marginBottom: 100
  },
  location: {
    position: 'relative',
    bottom: 0,
    backgroundColor: Colors.redColor,
    borderTopLeftRadius: 60,
    borderTopRightRadius: 60,
    width: Dims.DeviceWidth,
  },
  location1: {
    position: 'absolute',
    flex: 1,
    bottom: 0,
    top: 30,
    backgroundColor: Colors.Primary,
    borderTopLeftRadius: 60,
    borderTopRightRadius: 60,
    height: 100,
    width: Dims.DeviceWidth,
    marginBottom: 100
  },
  loginBtn: {
    width: Dims.DeviceWidth - 50,
    paddingVertical: 10,
    borderRadius: 2,
    backgroundColor: Colors.white
  },
  confirmButton: {
    width: Dims.DeviceWidth - 250,
    paddingVertical: 10,
    borderRadius: 2,
    backgroundColor: Colors.Primary,
    marginTop: -70
  },
  container2: {
    alignSelf: 'center'
  },
  gradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  btn: {
    width: 170,
    height: 50,
    alignSelf: 'flex-end',
    marginHorizontal: 20
  },
  loginText: {
    color: Colors.Primary,
    alignSelf: 'center',
    // justifyContent: 'center',
    fontFamily: 'Poppins-Medium',
    letterSpacing: 3,
    textAlign: 'center',
  },
  closePanel: {
    flexWrap: 'wrap',
    // top: 20

  },
  labelStyle: {
    color: Colors.textColor,
    fontSize: 10,
    fontFamily: 'Poppins-Bold',
    borderBottomColor: Colors.White,
    letterSpacing: 1
  },
  floatingLabelView: {
    marginHorizontal: 20,
  },
  hidePasswordText: {
    fontSize: 16,
    textAlign: 'center', fontFamily: 'Poppins-Medium',
    textAlignVertical: 'center',
    // color:Colors.placeholderColor
    fontSize: 16,

  },
  inputStyle: {
    borderWidth: 0,
    // margin: 10,
    height: 45,
    fontFamily: 'Poppins-Medium',
    fontSize: 12,
    borderBottomColor: Colors.white,
    borderBottomWidth: 1,
    color: Colors.White

  }, busIcon: {
    // paddingTop: 10,
    color: Colors.Primary
  },
  inputText: {
    fontFamily: 'Poppins-Medium',
    left: 10,
    top: 5
  },
  max: {
    flex: 1,
  },
  buttonHolder: {
    height: 100,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: '#0093E9',
    borderRadius: 25,
  },
  buttonText: {
    color: '#fff',
  },
  fullView: {
    width: Dims.DeviceWidth,
    height: Dims.DeviceHeight - 100,
  },
  remoteContainer: {
    width: '100%',
    height: 150,
    position: 'absolute',
    top: 5
  },
  remote: {
    width: 150,
    height: 150,
    marginHorizontal: 2.5
  },
  noUserText: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: '#0093E9',
  },
  container: {
    flex: 1,
  },
  float: {
    width: '100%',
    position: 'absolute',
    alignItems: 'center',
    bottom: 20,
  },
  top: {
    width: '100%',
  },
  input: {
    borderColor: 'gray',
    borderWidth: 1,
  },

  fullscreen: {
    width: Dims.DeviceWidth,
    height: Dims.DeviceHeight,
  },
});