import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, Alert, TextInput, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
var moment = require('moment');

class Home extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            locationAddress: params && params.locationAddress ? params.locationAddress : [],
            getData: params && params.getData ? params.getData : [],
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Location')
        return true;
    };
    renderSideMenuContent = () => {
        return (
            <>
                <ScrollView>
                    <Text>'Hi'</Text>
                </ScrollView>
            </>
        );
    };
    renderMainContent = () => {
        if (!this.state.drawerOpen) {
            return (
                <View>
                    <Entypo name="menu" size={38} color={CustomColors.Black} />
                </View>
            );
        }
    };
    _renderItem = (item) => {
        return (
            <Card style={{ marginBottom: 10 }}>
                <Text style={{ textAlign: 'right', color: Colors.Primary, fontFamily: 'Poppins-Medium', marginRight: Dims.DeviceWidth * 0.020, top: 10, fontSize: 12, paddingHorizontal: 10 }}>in {item.time}</Text>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('SubScription')} style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
                    <Text style={{ alignSelf: 'center', width: 50 }}>
                        <FontAwesome
                            name="user-circle"
                            size={30}
                            color={Colors.placeholderColor}
                            style={{}}
                        /></Text>
                    <View style={{ flexDirection: 'column', }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 1, }}>{item.name} <Text style={{ color: Colors.greyColor }}>Age: {item.age}</Text></Text>
                        </View>
                        <View style={{ flexDirection: 'row', top: 10 }}>
                            <Text style={{ alignSelf: 'center', width: 30 }}>
                                <MaterialIcons
                                    name="add-location"
                                    size={25}
                                    color={Colors.lightOrangeColor}
                                    style={{}}
                                />{'    '}</Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.greyColor }}> locate ( {item.distance} )</Text>
                            <Text style={{ alignSelf: 'center', width: 100, textAlign: 'right', fontFamily: 'Poppins-Medium' }}>
                                <Ionicons
                                    name="call"
                                    size={20}
                                    color={Colors.Primary}
                                    style={{}}
                                />{' '} call</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </Card>
        )
    }


    render() {
        let { locationAddress, getData } = this.state;
        let locatedAddress = [locationAddress.locality, locationAddress.country, locationAddress.adminArea]
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <ScrollView style={{ height: Dims.DeviceHeight }}>
                    <View style={{ height: 150, backgroundColor: Colors.Primary, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Location')}>
                            <Text style={{ alignSelf: 'flex-start', marginLeft: 10, padding: 3, top: 5 }}>
                                <MaterialCommunityIcons
                                    name="arrow-left"
                                    size={25}
                                    color={Colors.White}
                                    style={{}}
                                /></Text>
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'column', paddingVertical: 5, alignSelf: 'flex-start', padding: 3 }}>
                            <Text style={{ textAlign: 'center', color: Colors.White, fontFamily: 'Poppins-Medium', fontSize: 16, letterSpacing: 0.8, top: 3 }}>ORDER STATUS</Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.White, marginTop: 30, letterSpacing: 0.1 }}> Request No:MEDEXFORCE {getData ? getData.refNo : ''} </Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.White }}> Date:{getData ? getData.appointmentDate : ''} </Text>
                        </View>
                        <View style={{ flexDirection: 'column', margin: 10, padding: 3 }}>
                            <Text style={{ alignSelf: 'flex-start', }}>
                                <FontAwesome5
                                    name="headset"
                                    size={20}
                                    color={Colors.White}
                                    style={{}}
                                /></Text>
                        </View>
                    </View>
                    <Card style={{ height: 220, backgroundColor: 'white', width: Dims.DeviceWidth / 1.4, alignSelf: 'center', marginTop: -20 }}>
                        <View style={{ alignSelf: 'center', margin: 20 }}>
                            <Image style={{ height: 70, width: 70, alignSelf: 'center', }}
                                resizeMode={'contain'}
                                source={require("../assets/images/icon2.png")} />
                            <Text style={{ color: Colors.Primary, textAlign: 'center', fontSize: 16, fontFamily: 'Poppins-Bold', letterSpacing: 0.4 }}>{getData ? getData.name : ''}</Text>
                            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                <Image style={{ height: 14, width: 14 }}
                                    source={require('../assets/images/like.png')} />
                                <Text style={{ color: Colors.greyColor, fontFamily: 'Poppins-Medium', fontSize: 10, textAlign: 'center', left: 5 }}>Served 300 patients</Text>
                            </View>
                        </View>
                        <Text style={{ borderWidth: 0.4, borderColor: Colors.greyColor, height: 0.2, marginHorizontal: 30 }}></Text>
                        <Text style={{ color: Colors.darkGreyColor, fontFamily: 'Poppins-Bold', fontSize: 12, textAlign: 'center', marginTop: 10 }}>{getData ? getData.role : ''}</Text>
                        <Text style={{ color: Colors.greyColor, fontFamily: 'Poppins-Medium', fontSize: 10, textAlign: 'center', }}>{getData ? getData.description : ''}</Text>
                    </Card>
                    <View style={{ height: 40, borderWidth: 1, borderColor: Colors.Primary, width: 1, alignSelf: 'center' }} />
                    <Card style={{ height: 100, backgroundColor: 'white', width: Dims.DeviceWidth / 1.4, alignSelf: 'center', marginTop: 13 }}>
                        <View style={{ fontFamily: 'Poppins-Medium', fontSize: 8, alignSelf: 'center', height: 30, width: 30, borderRadius: 30, borderColor: Colors.Primary, borderWidth: 1, top: -15 }}>
                            <Text style={{ textAlign: 'center', justifyContent: 'center', top: 5, }}><MaterialCommunityIcons
                                name="coffee"
                                size={15}
                                color={Colors.Primary}
                                style={{}}
                            /></Text></View>
                        <View style={{ alignSelf: 'center' }}>
                            <Text style={{ color: Colors.greyColor, textAlign: 'center', fontSize: 13, fontFamily: 'Poppins-Medium', letterSpacing: 0.4 }}>Appoinment  Confirmation</Text>
                            <Text style={{ color: Colors.Primary, textAlign: 'center', fontSize: 16, fontFamily: 'Poppins-Bold', letterSpacing: 0.4 }}>CONFIRMED</Text>
                        </View>
                    </Card>
                    <View style={{ height: 40, borderWidth: 1, borderColor: Colors.Primary, width: 1, alignSelf: 'center' }} />
                    <Card style={{ height: 100, backgroundColor: 'white', width: Dims.DeviceWidth / 1.4, alignSelf: 'center', marginTop: 13 }}>
                        <View style={{ fontFamily: 'Poppins-Medium', fontSize: 8, alignSelf: 'center', height: 30, width: 30, borderRadius: 30, borderColor: Colors.Primary, borderWidth: 1, top: -15 }}>
                            <Text style={{ textAlign: 'center', justifyContent: 'center', top: 5, }}><MaterialIcons
                                name="confirmation-num"
                                size={15}
                                color={Colors.Primary}
                                style={{}}
                            /></Text></View>
                        <View style={{ alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <Image style={{ height: 50, width: 50, alignSelf: 'center', }}
                                    resizeMode={'contain'}
                                    source={require("../assets/images/icon2.png")} />
                            </View>
                            <View style={{ left: 10 }}>
                                <Text numberOfLines={2} style={{ color: Colors.greyColor, textAlign: 'center', fontSize: 13, fontFamily: 'Poppins-Medium', letterSpacing: 0.1 }}>{getData ? getData.name : ''} is on {getData && getData.gender === "Female" ? "her" : "his"} way</Text>
                                <Text numberOfLines={2} style={{ color: Colors.Primary, textAlign: 'center', fontSize: 16, fontFamily: 'Poppins-Bold', letterSpacing: 0.4 }}>TODAY AT 6.15 PM</Text>
                            </View>
                        </View>
                    </Card>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10, top: 20 }}>
                        <View style={{ height: 40, backgroundColor: Colors.Primary, flexDirection: 'row', flex: 0.3, alignSelf: 'center', justifyContent: 'center', borderRadius: 10 }}>
                            <Text style={{ textAlign: 'center', alignSelf: 'center', }}><MaterialIcons
                                name="call"
                                size={15}
                                color={Colors.White}
                                style={{}}
                            /></Text>
                            <Text style={{ fontSize: 12, textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold', color: Colors.white, letterSpacing: 0.2 }}> {'  '}CALL</Text>
                        </View>
                        <View style={{ height: 40, backgroundColor: 'orange', flexDirection: 'row', flex: 0.3, alignSelf: 'center', justifyContent: 'center', borderRadius: 10 }}>
                            <Text style={{ textAlign: 'center', alignSelf: 'center', }}><MaterialIcons
                                name="add-location"
                                size={15}
                                color={'white'}
                                style={{}}
                            /></Text>
                            <Text style={{ fontSize: 12, textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold', color: Colors.white, letterSpacing: 0.2 }}> {'  '}LOCATE</Text>
                        </View>
                        <View style={{ height: 35, backgroundColor: 'red', flexDirection: 'row', flex: 0.3, alignSelf: 'center', justifyContent: 'center', borderRadius: 10 }}>
                            <Text style={{ textAlign: 'center', alignSelf: 'center', }}><AntDesign color={Colors.White} size={15} name="closecircle" /></Text>
                            <Text style={{ fontSize: 12, textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold', color: Colors.white, letterSpacing: 0.2 }}> {'  '}CANCEL</Text>
                        </View>
                    </View>
                    <View style={{ height: 100 }}></View>
                </ScrollView>
                <Footer navigation={this.props.navigation} discountData={this.state.discountData} nearByData={this.state.nearByShopData} />
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        height: 120,
        width: 120,
        borderRadius: 100,
    },
    container: {
        flexDirection: 'row',
    },
});

export default Home;
