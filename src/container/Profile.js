import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, ActivityIndicator, TouchableOpacity, ScrollView, Alert, TextInput, Modal, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import { HttpHelper, HttpFavHelper, HttpMultiPartHelper } from '../HelperApi/Api/HTTPHelper';
import { LocationUrl, ProfileGetUrl, GetImageUrl, ExpertiseUrl } from '../HelperApi/Api/APIConfig';
import Header from '../components/Header';
import FloatingLabel from 'react-native-floating-labels';
import TagInput from 'react-native-tags-input';
import ImagePicker from 'react-native-image-picker';
import base64 from 'react-native-base64'

const options = {
    title: 'Chagua Avatar',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
var moment = require('moment');
import Geocoder from 'react-native-geocoder';
import { formValues } from 'redux-form';
class Profile extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            locationAddress: params && params.locationAddress ? params.locationAddress : [],
            getExpertiseData: [],
            showWorkInfo: false,
            selectWorkId: '',
            profilePic: {},
            tags: {
                tag: '',
                tagsArray: []
            },
            tagsColor: Colors.textColor,
            tagsText: '#EEEEE',
            aboutMe: '',
            workingAt: '',
            designation: '',
            isModalVisible: false,
            isOpacity: false,
            isFocus: false,
            data: new FormData(),
            isLoading: false,
            isLoadingSubmit: false,
            expertiseArray: []
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        AsyncStorage.getItem('login', (err, login) => {
            if (login != null) {
                var keyData = JSON.parse(login);
                this.setState({
                    accessKey: keyData.accessToken,
                    personId: keyData.personId,
                    isLoading: true
                }, () => {
                    this.getProfile()
                    this.getLocation()
                    this.getExpertise()
                })
            }
        })
    }
    getProfile = () => {
        let profileData = HttpFavHelper(`${ProfileGetUrl}`, 'GET', '', this.state.accessKey);
        profileData.then(response => {
            if (response)
                this.setState({
                    profileData: response,
                    name: response.firstName + ' ' + response.lastName,
                    toggle: response.availability,
                    designation: response.designation,
                    workingAt: response.workingAt,
                    selectWorkIduId: response.workingAt,
                    aboutMe: response.description,
                    tags: {
                        tagsArray: [response.tags]
                    },
                    isFocus: response ? true : false,
                    personImage: response && response.personProfilePic ? response.personProfilePic.personImage : '',
                    isLoading: false

                })
        })
        let imageData = HttpFavHelper(`${GetImageUrl}`, 'GET', '', this.state.accessKey);
        imageData.then(imageDataResponse => {
            if (imageDataResponse)
                this.setState({
                })
        })
    }
    getLocation = () => {
        let locationData = HttpHelper(`${LocationUrl}/${this.state.personId}`, 'GET', '');
        locationData.then(locationResponse => {
            if (locationResponse) {
                var dataLatLng = {
                    lat: Number(locationResponse.lattitude),
                    lng: Number(locationResponse.longitude)
                };
                Geocoder.geocodePosition(dataLatLng).then(responseJson => {
                    if (responseJson) {
                        this.setState(
                            {
                                locationAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                                selectAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                                getDataAddress: responseJson && responseJson[0] ? responseJson[0] : [],
                                isSelect: true,
                                position: responseJson[0].position,
                                isLoading: false
                            });
                    }
                })
                    .catch(err => console.log(err))
            }
        })
    }
    getExpertise = () => {
        let expertiseData = HttpFavHelper(`${ExpertiseUrl}`, 'GET', '', this.state.accessKey);
        expertiseData.then(response => {
            console.log(response, 'expertiseData');


            this.setState({
                getExpertiseData: response

            })
        })

    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    handleBackPress = () => {
        this.props.navigation.navigate('Home', { isSave: false })
        return true;
    };
    handleChange = (event, type) => {
        if (type === "des") {
            this.setState({ designation: event.nativeEvent.text });
        } else if (type === "about") {
            this.setState({ aboutMe: event.nativeEvent.text });
        }
    };
    updateTagState = (state) => {
        this.setState({
            tags: state
        })
    };
    handleDesignation = () => {
        return (
            <View style={[styles.inputWraps, { borderBottomColor: Colors.lightGreyColor, borderBottomWidth: 1, }]}>
                <View style={{ flex: 1 }}>
                    <FloatingLabel
                        labelStyle={[styles.labelStyle]}
                        inputStyle={styles.inputStyle}
                        onChange={event => this.handleChange(event, 'des')}
                        style={styles.floatingLabelView}
                        value={this.state.designation}
                    >
                        Designation
                    </FloatingLabel>
                </View>
            </View>
        )
    }
    handleWork = () => {
        let { showWorkInfo, selectWorkIduId } = this.state;
        return (
            <View style={{}}>
                <View
                    style={{
                        backgroundColor: Colors.DarkUnderline,
                        borderRadius: 5,
                    }}>
                    <TouchableOpacity style={styles.input} onPress={this.onWorkClick}>
                        {selectWorkIduId ? (
                            <>
                                <Text style={{
                                    color: Colors.blackColor,
                                    fontSize: 12,
                                    fontFamily: 'Poppins-Medium',
                                    letterSpacing: 1, left: 10, top: 10
                                }}>{selectWorkIduId}</Text>
                                <Text style={{ color: Colors.blackColor, top: 10 }}><AntDesign
                                    style={{}}
                                    name="caretdown"
                                    color={Colors.Text}
                                    size={12}
                                /></Text>
                            </>
                        ) : (
                            <>
                                <Text style={{
                                    color: Colors.blackColor,
                                    fontSize: 12,
                                    fontFamily: 'Poppins-Medium',
                                    letterSpacing: 1, left: 10, top: 10
                                }}>{"Expertise in"}</Text>
                                <Text style={{ color: Colors.blackColor, top: 10 }}><AntDesign
                                    style={{}}
                                    name="caretdown"
                                    color={Colors.Text}
                                    size={12}
                                /></Text>
                            </>
                        )}
                    </TouchableOpacity>
                    <View style={{ borderColor: Colors.lightGreyColor, borderWidth: 0.5 }}></View>
                    <ScrollView style={showWorkInfo ? styles.dropDown : { display: 'none' }}>
                        {this._renderPickerItem()}
                    </ScrollView>
                </View>
            </View >
        )
    }
    onWorkClick = () => {
        this.setState({ showWorkInfo: !this.state.showWorkInfo })
    }
    _renderPickerItem() {
        let {workingAt} = this.state
        if (
            this.state.getExpertiseData != null &&
            this.state.getExpertiseData.length > 0
        ) {
            return this.state.getExpertiseData.map(item => (
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <Text style={styles.text} onPress={() => this.updatePickerItem(item)} >
                    {item}
                </Text>
                <Text style={{ color: Colors.blackColor, top: 10,marginRight:10 }}><Ionicons
                                    style={{}}
                                    name={item ===workingAt ?"radio-button-on-outline" : "radio-button-off-sharp"}
                                    color={ Colors.white}
                                    size={12}
                                /></Text>

                </View>
            ));
        }
    }
    updatePickerItem = (item) => {
        this.setState({
            selectWorkIduId: item,
            showWorkInfo: false,
            workingAt: item
        })
    }
    handleAbout = () => {
        return (
            <View style={[styles.inputWraps, { borderBottomColor: Colors.lightGreyColor, borderBottomWidth: 1, }]}>
                <View style={{ flex: 1 }}>
                    <FloatingLabel
                        labelStyle={[styles.labelStyle]}
                        inputStyle={styles.inputStyle}
                        onChange={event => this.handleChange(event, 'about')}
                        style={styles.floatingLabelView}
                        value={this.state.aboutMe}
                    >
                        Few lines about you
                    </FloatingLabel>
                </View>
            </View>
        )
    }
    handleTag = () => {
        let { tags } = this.state;
        return (
            <View style={styles.customContainer}>
                <TagInput
                    updateState={this.updateTagState}
                    tags={tags}
                    label={!this.state.isFocus && !this.state.isFocus && 'Add tags that define your work and separate with commas.{eg.Head Nurse, Central Care etc ..}'}
                    labelStyle={{
                        color: Colors.blackColor, fontFamily: 'Poppins-Medium'
                    }}
                    leftElementContainerStyle={{ marginLeft: 3 }}
                    containerStyle={{ width: (Dims.DeviceWidth - 40) }}
                    inputContainerStyle={[styles.textInput, {}]}
                    inputStyle={{ color: this.state.tagsText }}
                    onFocus={() => this.setState({ isFocus: true })}
                    onBlur={() => this.setState({ tagsColor: Colors.Primary, })}
                    autoCorrect={true}
                    tagStyle={styles.tag}
                    tagTextStyle={styles.tagText}
                    keysForTag={','} />
            </View>
        )
    }
    selectImage = () => {
        let { data } = this.state;
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                this.setState({
                    profilePic: {
                        uri: response.uri,
                        name: response.fileName,
                        type: response.type,
                        fileSize: (response.fileSize / (1000)).toFixed(2),
                    },
                });
                const source = { uri: response.uri };
                this.setState(
                    {
                        avatarSource: source,
                        file: response,
                        isImageUpload: true,
                    },
                    () => {
                        if (this.state.profilePic.fileSize >= 600) {
                            this.setState({
                                profilePic: {},
                                file: {},
                                avatarSource: {}
                            }, () => {
                                this.dropdown.alertWithType('error', 'Error!', 'image size should be 600 kb')
                            })
                        } else {
                            data.append('file', {
                                uri: this.state.file.uri,
                                name: 'nature.jpg',
                                type: this.state.file.type,
                            });
                        }
                    },
                );
            }
        });
    };
    onSubmitHandle = () => {
        let { designation, workingAt, aboutMe, tags, accessKey, data, isLoadingSubmit } = this.state;
        console.log(tags.tagsArray,'tags.tagsArray');
        data.append('content', JSON.stringify({ "description": aboutMe, "designation": designation, "tags": tags.tagsArray, "workingAt": workingAt }));
        this.setState({ data, isLoadingSubmit: true })
        if (accessKey) {
            if (designation) {
                if (workingAt) {
                    if (aboutMe) {
                        if (tags && tags.tagsArray) {
                            console.log(data, ' this.state.data');
                            let profileData = HttpMultiPartHelper(`${ProfileGetUrl}/update`, 'POST', this.state.data, this.state.accessKey);
                            profileData.then(response => {
                                console.log(response, 'kkkkkkkkk');
                                this.setState({ isLoadingSubmit: true })
                                if (response) {
                                    this.setState({
                                        isModalVisible: true,
                                        isOpacity: true,
                                        isLoadingSubmit: false
                                    })
                                } else {
                                    this.dropdown.alertWithType('error', 'Error!', 'Server Error')
                                }
                            })
                        } else {
                            this.dropdown.alertWithType('error', 'Error!', 'Please enter tags')
                        }
                    } else {
                        this.dropdown.alertWithType('error', 'Error!', 'Please enter about you')
                    }
                } else {
                    this.dropdown.alertWithType('error', 'Error!', 'Please select workingAt')
                }
            } else {
                this.dropdown.alertWithType('error', 'Error!', 'Please enter designation')
            }
        }
        else {
            this.dropdown.alertWithType('error', 'Error!', 'Server Error')
        }
    }
    handleImage = () => {
        let { profilePic, personImage } = this.state;
        return (
            <TouchableOpacity onPress={() => this.selectImage()} style={{ borderRadius: 10, height: Dims.DeviceHeight / 4.8, borderWidth: 1, borderColor: Colors.Primary, width: Dims.DeviceWidth / 2, alignSelf: 'center', margin: 10 }}>
                {(personImage || profilePic.uri) ?
                    (
                        <View style={{ width: Dims.DeviceWidth / 2, height: Dims.DeviceHeight / 4.8, alignSelf: 'center', }}>
                            <Image source={profilePic && profilePic.uri ? { uri: profilePic.uri } : { uri: `data:image/jpeg;base64,${personImage}` }} style={{ width: Dims.DeviceWidth / 2, height: Dims.DeviceHeight / 4.8, }} />
                        </View>
                    ) : (
                        <>
                            <Text style={{ alignSelf: 'center', justifyContent: 'center', textAlign: 'center', marginTop: 20 }}><MaterialCommunityIcons

                                name="camera-plus"
                                color={Colors.Primary}
                                size={35}
                            /></Text>
                            <Text style={{ alignSelf: 'center', justifyContent: 'center', textAlign: 'center', margin: 20, fontFamily: 'Poppins-Medium', color: Colors.Primary }}>UPLOAD YOUR PICTURE</Text>
                        </>
                    )}
            </TouchableOpacity>
        )
    }
    handleNote = () => {
        let { isLoading } = this.state;
        return (
            <>
                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, textDecorationLine: 'underline' }}>PLEASE NOTE</Text>
                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 10, margin: 5 }}>1. Selfie should be  very taken under bright light and professional else your  profile  might get blacklisted.</Text>
                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 10, margin: 5 }}>2. 70& of the photo area should consist your face.</Text>
                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 10, margin: 5 }}>3. Do Not Upload group pictures.</Text>
            </>
        )
    }
    handleSubmit = () => {
        let { profilePic, isLoadingSubmit } = this.state;
        return (
            <TouchableOpacity onPress={() => this.onSubmitHandle()} style={{ borderRadius: 2, borderWidth: 1, height: 50, borderColor: Colors.Primary, width: Dims.DeviceWidth / 1.1, alignSelf: 'center', margin: 10, backgroundColor: Colors.Primary }}>
                <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 14, color: Colors.White, textAlign: 'center', alignSelf: 'center', padding: 10, letterSpacing: 1 }}>{isLoadingSubmit ? <ActivityIndicator size="large" color={Colors.White} />
                    : "SUBMIT"}</Text>
            </TouchableOpacity>
        )
    }
    onOkHandle = () => {
        this.setState({ isModalVisible: false, isOpacity: false }, () => {
            this.props.navigation.navigate('Home', { isSave: true })
        })
    }
    activeLoading = () => {
        let { isLoading } = this.state;
        if (isLoading) {
            return (
                <View
                    style={{
                        width: '10%',
                        height: '10%',
                        alignSelf: 'center',
                        justifyContent: 'center',
                    }}>
                    <ActivityIndicator size="large" color={Colors.Primary} />
                </View>
            );
        }
    }
    render() {
        let { locationAddress, isOpacity } = this.state;
        let locatedAddress = [locationAddress.locality, locationAddress.country, locationAddress.adminArea]
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <ScrollView style={isOpacity && { backgroundColor: Colors.Primary, opacity: 0.2 }}>
                    <Header navigation={this.props.navigation} isProfile={true} isArrow={true} page={'Home'} title={'Profile Activation'} />
                    <View style={{ padding: 10 }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.Primary, textAlign: 'center', fontSize: 16, letterSpacing: 0.7 }}>Yay! You are just one step away</Text>
                        <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.placeholderColor, textAlign: 'center', fontSize: 9 }}>Please add your detailed into for better reach</Text>
                        {this.activeLoading()}
                        {this.handleDesignation()}
                        {this.handleWork()}
                        {this.handleAbout()}
                        {this.handleTag()}
                        {this.handleImage()}
                        {this.handleNote()}
                        {this.handleSubmit()}
                    </View>
                    <View style={{ height: 30 }}></View>
                    <View style={styles.centeredView}>
                        <Modal
                            animated
                            animationType="slide"
                            transparent={true}
                            visible={this.state.isModalVisible}
                            style={{
                                opacity: 0.5,
                            }}
                            onRequestClose={() => {
                                this.setState({ isModalVisible: !this.state.isModalVisible, isOpacity: false })
                            }}>
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <View style={{ height: Dims.DeviceHeight / 2.5, width: Dims.DeviceWidth - 40, borderTopLeftRadius: 60, borderTopRightRadius: 60, alignSelf: 'center' }}>
                                        <Image source={require('../assets/images/payment.png')} style={{ alignSelf: 'center', height: 120, width: 120 }} />
                                        <Text style={{ fontFamily: 'Poppins-Bold', textAlign: 'center', margin: 10, fontSize: 15 }}>Profile Updated SuccessFully</Text>
                                        <Text style={{ fontFamily: 'Poppins-Medium', textAlign: 'center', margin: 10, fontSize: 13 }}>Please continue to  your offer services.</Text>
                                        <TouchableOpacity style={{ backgroundColor: Colors.orangeColor, width: 150, borderRadius: 30, alignSelf: 'center', top: 10 }} onPress={() => { this.onOkHandle() }}>
                                            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10 }}>Continue</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>
                </ScrollView>
                <Footer navigation={this.props.navigation} discountData={this.state.discountData} nearByData={this.state.nearByShopData} profile={true} />
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );

    }
}

const styles = StyleSheet.create({
    inputWraps: {
        flexDirection: 'row',
        flex: 1,
    },
    labelStyle: {
        fontSize: 12,
        fontFamily: 'Poppins-Medium',
        letterSpacing: 1,
        color: Colors.blackColor
    },
    inputStyle: {
        borderWidth: 0,
        fontFamily: 'Poppins-Medium',
        fontSize: 12,
        color: Colors.blackColor
    },
    floatingLabelView: {
        // marginHorizontal: 20,
    },
    input: {
        height: 50,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        top: 10
    },
    dropDown: {
        width: '100%',
        borderTopWidth: 0,
        textAlign: 'center',
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        backgroundColor: Colors.Primary,
        height: 'auto',
        zIndex: 999999999
    },
    text: {
        padding: 10,
        fontFamily: 'Poppins-Medium',
        color: Colors.white
    },
    standardContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    customContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        color: Colors.placeholderColor,
        marginTop: 10,
        borderWidth: 1,
        borderColor: Colors.lightGreyColor
    },
    textInput: {
        height: 40,
        borderColor: 'white',
        marginTop: 8,
        borderRadius: 5,
        padding: 3,
    },
    tag: {
        backgroundColor: '#fff',
    },
    tagText: {
        fontFamily: 'Poppins-Medium'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
    },
    modalView: {
        marginHorizontal: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        shadowColor: Colors.greyColor,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
});

export default Profile;
