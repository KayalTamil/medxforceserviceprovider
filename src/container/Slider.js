import React, { Component, useState } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, Alert, TextInput, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch, requireNativeComponent
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import Swiper from 'react-native-swiper';
import SplashScreen from 'react-native-splash-screen';
var moment = require('moment');
var sliderData = [{
    id: '1',
    latitude: 17.4317,
    longitude: 78.5654,
    km: 6,
    name: 'Mrs. Tamizh',
    image: require("../assets/images/slider1.png"),
    appointmentDate: '10/01/2022',
    refNo: '#202209501',
    gender: 'Female',
    role: 'Head Nurse Medical Travel Assistant',
    description: "She is friendly, caring and punctual."
},
{
    id: '2',
    latitude: 17.4657,
    longitude: 78.4283,
    km: 10,
    name: 'Mrs. Srinija',
    image: require("../assets/images/slider1.png"),
    appointmentDate: '12/01/2022',
    refNo: '#202209502',
    gender: 'Female',
    role: 'Head Nurse',
    description: "She is friendly, caring and punctual"
},
{
    id: '3',
    latitude: 17.3990,
    longitude: 78.4867,
    km: 2,
    name: 'Mr. Arjun',
    image: require("../assets/images/slider1.png"),
    appointmentDate: '09/01/2022',
    refNo: '#202209503',
    gender: 'Male',
    role: "Medical Travel Assistant",
    description: "He is friendly, caring and punctual."
},

]


class Home extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            locationAddress: params && params.locationAddress ? params.locationAddress : [],
            getData: params && params.getData ? params.getData : [],
            getBannerImage: sliderData
        };
    }

    componentDidMount() {
        SplashScreen.hide();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        SplashScreen.hide();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Location')
        return true;
    };
    renderSideMenuContent = () => {
        return (
            <>
                <ScrollView>
                    <Text>'Hi'</Text>
                </ScrollView>
            </>
        );
    };
    renderMainContent = () => {
        if (!this.state.drawerOpen) {
            return (
                <View>
                    <Entypo name="menu" size={38} color={Colors.Black} />
                </View>
            );
        }
    };
    getBannerImage = () => {
        let { getBannerImage } = this.state;
        if (getBannerImage && getBannerImage.length > 0) {
            return (
                <View><Text>Hi</Text></View>
            )
        }
    }
    _renderItem = (item) => {
        return (
            <Card style={{ marginBottom: 10 }}>
                <Text style={{ textAlign: 'right', color: Colors.Primary, fontFamily: 'Poppins-Medium', marginRight: Dims.DeviceWidth * 0.020, top: 10, fontSize: 12, paddingHorizontal: 10 }}>in {item.time}</Text>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('SubScription')} style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
                    <Text style={{ alignSelf: 'center', width: 50 }}>
                        <FontAwesome
                            name="user-circle"
                            size={30}
                            color={Colors.placeholderColor}
                            style={{}}
                        /></Text>
                    <View style={{ flexDirection: 'column', }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 1, }}>{item.name} <Text style={{ color: Colors.greyColor }}>Age: {item.age}</Text></Text>
                        </View>
                        <View style={{ flexDirection: 'row', top: 10 }}>
                            <Text style={{ alignSelf: 'center', width: 30 }}>
                                <MaterialIcons
                                    name="add-location"
                                    size={25}
                                    color={Colors.lightOrangeColor}
                                    style={{}}
                                />{'    '}</Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.greyColor }}> locate ( {item.distance} )</Text>
                            <Text style={{ alignSelf: 'center', width: 100, textAlign: 'right', fontFamily: 'Poppins-Medium' }}>
                                <Ionicons
                                    name="call"
                                    size={20}
                                    color={Colors.Primary}
                                    style={{}}
                                />{' '} call</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </Card>
        )
    }
    onHandleNavigate = (item, index) => {
        if (index === 0) {
            this.props.navigation.navigate('Location')
        }
    }

    render() {
        let { getBannerImage } = this.state;
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <Swiper
                    autoplay
                    autoplayTimeout={5}
                    activeDot={
                        <View
                            style={{
                                backgroundColor:
                                    Colors.Primary,
                                width: 6,
                                height: 6,
                                borderRadius: 4,
                                marginLeft: 3,
                                marginRight: 3,
                                marginTop: 3,
                                marginBottom: 3,
                                flexDirection: 'row'
                            }}
                        />
                    }
                >
                    {(
                        getBannerImage && getBannerImage.map((item, index) => {
                            return (
                                <TouchableOpacity disabled={index == 0 ? false : true} onPress={() => { this.onHandleNavigate(item, index) }} style={{ height: Dims.DeviceHeight }}>
                                    <Image
                                        style={{
                                            height: Dims.DeviceHeight,
                                            width: Dims.DeviceWidth
                                        }}
                                        source={item.image}
                                    />
                                </TouchableOpacity>
                            )

                        })
                    )}
                </Swiper>
            </>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        height: 120,
        width: 120,
        borderRadius: 100,
    },
    container: {
        flexDirection: 'row',
    },
});

export default Home;
