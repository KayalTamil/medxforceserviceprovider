import 'react-native-gesture-handler';
import React, { Component } from 'react';

import { createAppContainer, createSwitchNavigator } from 'react-navigation';
// Import Screens
import Login from '../src/auth/Login';
import Register from '../src/auth/Register';
import LoginVerify from '../src/auth/LoginVerify';
import SubScription from '../src/auth/SubScription';
import Home from '../src/container/Home';
import Location from '../src/container/Location';
import RegisterLocation from '../src/auth/registerLocation';
import MapLocation from '../src/container/MapLocation';
import SearchLoaction from '../src/container/SearchLocation';
import OrderStatus from '../src/container/OrderStatus';
import Slider from '../src/container/Slider';
import History from '../src/container/History';
import Dash from '../src/auth/Dash';
import Profile from '../src/container/Profile';
import Agora from '../src/container/AgoraSdk';

const App = createSwitchNavigator({
    Dash: {
        screen: Dash,
    },
    Login: {
        screen: Login,
    },
    Agora: {
        screen: Agora,
    },
    Home: {
        screen: Home,
    },
    SubScription: {
        screen: SubScription,
    },
    Profile: {
        screen: Profile,
    },
    Location: {
        screen: Location,
    },
    SubScription: {
        screen: SubScription,
    },
   
    SubScription: {
        screen: SubScription,
    },
    Login: {
        screen: Login,
    },
    Location: {
        screen: Location,
    },
    History: {
        screen: History,
    },
    Slider: {
        screen: Slider,
    },
    OrderStatus: {
        screen: OrderStatus,
    },
    Location: {
        screen: Location,
    },
    LoginVerify: {
        screen: LoginVerify,
    },
    Location: {
        screen: Location,
    },
    RegisterLocation: {
        screen: RegisterLocation,
    },
    Register: {
        screen: Register,
    },
})

export default createAppContainer(App);
